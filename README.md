paxelfica

# la liste des PNJs de la campagne Pax Elfica
# vous retrouverez aussi tous les pré-tirés.

Il vous faudra bien sur la Campagne Pax Elfica éditée par les Douze Singes pour obtenir le texte, les Battlemaps et les illustrations.

## pour jouer avec plus de confort

vous pouvez installer la liste de module suivant :

* fr-FR : permet la traduction des interfaces de foundryVTT
* dnd5e_fr-FR : permet la traduction du system dnd5e
* babele permet la traduction des compendiums du systeme DnD5e
* SRD heros et Dragons

